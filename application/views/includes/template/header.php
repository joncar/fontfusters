<header class="row header navbar-static-top" id="main_navbar">
    <div class="container">
        <div class="row m0 social-info">
            <ul class="social-icon">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                <li class="tel"><a href="tel:+1234567890"><i class="fa fa-phone"style="color: white;"></i> <a href="tel:+34938035131"style="color: white;">+34 93 803 51 31 </a></li>
                <li class="email"><a href="#"><i class="fa fa-envelope-o" style="color: white;"></i> <a href="mailto:info@fontfusters.cat" style="color: white;">email font@fontfusters.cat</a></li>
            </ul>
        </div>
    </div>
   <div class="logo_part">
        <div class="logo">
            <a href="<?= site_url() ?>" class="brand_logo">
                <img src="<?= base_url() ?>img/header/logo.png" alt="logo image">
            </a>
        </div>
    </div>
    <div class="main-menu">
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Collect the nav links, forms, and other content for toggling -->
            
            <div class="menu row m0">                
                <ul class="nav navbar-nav navbar-right visible-sm">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="icon icon-Search"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                <form action="#" method="get" class="search-form">
                                    <input type="search" class="form-control" placeholder="Escriu i enter">
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="<?= site_url() ?>">Inici</a>
                        </li>
                        <li><a href="<?= site_url() ?>lempresa.html">Empresa</a></li>
                        <li class="dropdown">
                            <a href="<?= site_url('serveis') ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Serveis</a>
                            <ul class="dropdown-menu">
                                <?php foreach($this->db->get('servicios')->result() as $s): ?>
                                <li><a href="<?= site_url('serveis/'.toURL($s->id.'-'.$s->titulo)) ?>"><?= $s->titulo ?></a></li>                                    
                                <?php endforeach ?>
                            </ul>
                        </li>
                        <li>
                            <a href="<?= site_url('projectes') ?>">Projectes</a>
                            <ul class="dropdown-menu">
                                <?php foreach($this->db->get('proyectos')->result() as $n=>$v): ?>
                                    <li><a href="<?= base_url('projectes/'.toURL($v->id.'-'.$v->titulo)) ?>"><?= $v->titulo ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="<?= site_url('blog') ?>">NOTÍCIES</a>
                        </li>
                        <li><a href="<?= site_url() ?>contacto.html">contacte</a></li>
                        <li class="dropdown">
                            <a href="<?= site_url('fichets') ?>">
                                <img src="<?= base_url() ?>img/logo-fichet.png" style=" width: 85px;">
                            </a>
                            <ul class="dropdown-menu">
                                <?php foreach($this->db->get('fichet')->result() as $s): ?>
                                    <li><a href="<?= site_url('fichets/'.toURL($s->id.'-'.$s->titulo)) ?>"><?= $s->titulo ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right hidden-xs hidden-sm">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="icon icon-Search"></i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <form action="#" method="get" class="search-form">
                                        <input type="search" class="form-control" placeholder="Escriu i enter">
                                    </form>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?= site_url('pressupost') ?>">
                                <i class="icon icon-Edit"></i></a>
                            </a>
                        </li>

                    </ul>
                </div>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
</header>