<!--great-work-->
<section class="emergency-contact">
    <div class="left-side">
        <div class="content">
            <h3>Demani pressupost sense compromís</h3>
            <h3>Envia'ns per mail una fotografia del que voldries fer i ens avançarem en el pressupost!</h3>
            <a href="tel:+34938035131" class="phone"><img src="<?= base_url() ?>img/great-work/phone.png" alt="">+34 93 803 51 31</a>
            <a href="<?= base_url() ?>pressupost" class="email"><img src="<?= base_url() ?>img/great-work/email.png" alt="">Demanar Pressupost</a>
        </div>
    </div>
    <div class="right-side">
        <img src="<?= base_url() ?>img/great-work/right-img.jpg" alt="">
    </div>
</section>
<!--footer-->
<footer class="row">
    <div class="row m0 footer-top">
        <div class="container">
            <div class="row footer-sidebar">
                <div class="widget about-us-widget col-sm-6 col-lg-3">
                    <a href="index.html" class="brand_logo">
                        <img src="<?= base_url() ?>img/header/logo.png" alt="logo image">
                    </a>
                    <p>Font Fusters, S.L és una empresa jove però de llarga tradició, sent la quarta generació de fusters.

L’actual empresa, fundada l’any 1996, es dedica tant a la fusteria (interior i exterior) com a l’ebenisteria en general, de taller i d’obra.</p>
                    <div class="social-icon row m0">
                        <ul class="nav">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>  
                        </ul>
                    </div>
                </div>
                <div class="widget widget-links col-sm-6 col-lg-3">
                    <h4 class="widget_title">els nostres serveis</h4>
                    <div class="widget-contact-list row m0">
                        <ul>
                            <?php foreach($this->db->get('servicios')->result() as $s): ?>
                                <li><a href="<?= site_url('serveis'.toURL($s->id.'-'.$s->titulo)) ?>"><i class="fa fa-angle-right"></i><?= $s->titulo ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="widget widget-contact  col-sm-6 col-lg-3">
                    <h4 class="widget_title">dades de contacte</h4>
                    <div class="widget-contact-list row m0">
                       <ul>
                            <li>
                                <i class="fa fa-map-marker"></i>
                                <div class="fleft location_address">
                                    Avinguda Europa, 6, Nau 2, 08700 Igualada. BARCELONA. Catalunya
                                </div>
                                
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <div class="fleft contact_no">
                                    <a href="#">+34 93 803 51 31</a>
                                </div>
                            </li>
                            <li>
                                <i class="fa fa-envelope-o"></i>
                                <div class="fleft contact_mail">
                                    <a href="#">font@fontfusters.cat</a>
                                </div>
                            </li>
                            <li>
                                <i class="icon icon-WorldWide"></i>
                                <div class="fleft service_time">
                                    Dil - Dij : 9h a 13h / 15h a 19h<br>Divendres : 9h a 13h / 15h a 18h<br>Dissabtes Tancat
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="widget widget4 widget-form col-sm-6 col-lg-3">
                    <h4 class="widget_title">deixa un missatge</h4>
                    <div class="widget-contact-list row m0">
                        <form class="submet-form row m0" action="#" method="post">
                            <input type="text" class="form-control" id="name" placeholder="Nom">
                            <input type="email" class="form-control" id="email" placeholder="Email">
                            <textarea class="form-control message" placeholder="Missatge"></textarea>
                            <button class="submit" type="submit">enviar</button>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>
     </div>
     <div class="row m0 footer-bottom">
         <div class="container">
            <div class="row">
               <div class="col-sm-8">
                   Copyright &copy; <a href="index.html">FONT FUSTERS</a> 2017. <br class="visible-xs"> Tots els drets reservats.
               </div>
               <div class="right col-sm-4">
                   Disseny x <a href="http://themeforest.net/user/designarc">Hipo</a>
               </div>
            </div>
        </div>
     </div>
</footer>