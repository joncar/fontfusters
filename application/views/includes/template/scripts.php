<script src="<?= base_url() ?>js/template/jquery-2.2.0.min.js"></script>
<script src="<?= base_url() ?>js/template/bootstrap.min.js"></script>
<!--RS-->	
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/jquery.themepunch.tools.min.js"></script> <!-- Revolution Slider Tools -->
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> <!-- Revolution Slider -->
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="<?= base_url() ?>css/vendors/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>

<script src="<?= base_url() ?>theme/vendors/jquery-ui-1.11.4/jquery-ui.min.js"></script>


<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="<?= base_url() ?>js/template/gmaps.min.js"></script>


<script src="<?= base_url() ?>css/vendors/isotope/isotope.min.js"></script>
<script src="<?= base_url() ?>css/vendors/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="<?= base_url() ?>css/vendors/owlcarousel/owl.carousel.min.js"></script>
<script src="<?= base_url() ?>css/vendors/magnific/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url() ?>js/template/theme.js"></script>

<?php if(!empty($js_files)): ?>
<?php foreach($js_files as $file): ?>
<script src="<?= $file ?>"></script>
<?php endforeach; ?>                
<?php endif; ?>

<script>
    $(document).on('ready',function(){
        $(".flexigrid input, .flexigrid textarea").on('keyup',function(){
        	var name = $(this).attr('name');
        	$('#'+name+'Label').html($(this).val());
        });
    });
</script>