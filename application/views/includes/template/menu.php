    <div id="start-screen_content-container">
        <div class="start-screen__content start-screen__content-first">
            <div class="v-align">
                <div class="v-middle">
                    <div class="container">
                        <div class="row flex-items-xs-center">
                            <div class="col-xs-12 col-xl-10">
                                <p class="title">Airfoc</p>
                                <p class="subtitle">Material Contraincedis</p>
                                <p>
                                    <a class="custom-btn big primary" href="#contacte">Contactar</a>
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="start-screen__content start-screen__content-first">
            <div class="v-align">
                <div class="v-middle">
                    <div class="container">
                        <div class="row flex-items-xs-center text-center">
                            <div class="col-xs-12 col-xl-10">
                                <p class="title">Airfoc</p>
                                <p class="subtitle">Prevenció d'incendis <br> a Catalunya</p>
                                <p>
                                    <a class="custom-btn big primary" href="#contacte">Contactar</a><br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="start-screen__content start-screen__content-second">
            <div class="v-align">
                <div class="v-middle">
                    <div class="container">
                        <div class="row flex-items-md-center text-center">
                            <div class="col-xs-12 col-lg-10 col-xl-8">
                                <p class="title">airfoc</p>
                                <p>Material Contraincedis empresa subministradora, instaladora i de mantenimient de qualsevol tipus de material i equip contra incendis en qualsevol dels seus vessants.&nbsp;</p>
                                <p>
                                    <a class="custom-btn big primary" href="#contacte">Contactar</a>
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="start-screen__content start-screen__content-third">
            <div class="v-align">
                <div class="v-middle">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-md-8">
                                <p class="title" style="color: white;" data-mce-style="color: white;">airfoc</p>
                                <p class="subtitle">Més de 30 anys d'experincència prevenint incendis</p>
                                <p>
                                    <a class="custom-btn big primary" href="#contacte">Contactar</a>
                                    <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>