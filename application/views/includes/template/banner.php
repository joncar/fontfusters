<!--rv-slider-->
<section class="bannercontainer row">
    <div class="rev_slider banner row m0" id="rev_slider" data-version="5.0">
        <ul>
            <li data-transition="slidehorizontal"  data-delay="10000">
                <img src="<?= base_url() ?>img/slider-img/bg.jpg"  alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                <div class="tp-caption sfr tp-resizeme carpenters-h1" 
                    data-x="0" data-hoffset="690" 
                    data-y="255" data-voffset="160" 
                    data-whitespace="nowrap"
                    data-start="900">
                    som Professionals

                </div>
                <div class="tp-caption sfb tp-resizeme carpenters-h2" 
                    data-x="0" data-hoffset="690" 
                    data-y="310" data-voffset="350" 
                    data-whitespace="nowrap"
                    data-start="1200">
                    que estimem molt la fusta
                </div>
                <div class="tp-caption sfb tp-resizeme carpenters-ul" 
                    data-x="0" data-hoffset="690" 
                    data-y="375" data-voffset="470" 
                    data-whitespace="nowrap"
                    data-start="1500">
                    <ul class="nav">
                        <li><a href="#">Disseny</a></li>
                        <li><a href="#">Comercial</a></li>
                        <li><a href="#">Industrial</a></li>
                    </ul>
                </div>
                <div class="tp-caption sfb tp-resizeme carpenters-p" 
                    data-x="0" data-hoffset="500" 
                    data-y="430" data-voffset="470" 
                    data-whitespace="nowrap"
                    data-start="1800">                    
                    Treballem la fusta a casa teva, al teu negoci...<br>Estem encantats de cumlir els teus somnis
                </div>
                <div class="tp-caption sfb tp-resizeme carpenters-b" 
                    data-x="0" data-hoffset="690" 
                    data-y="550" data-voffset="555" 
                    data-whitespace="nowrap"
                    data-start="2000">                    
                    <a href="<?= base_url() ?>p/pressupost.html" class="btn btn-2 submit">DEmana pressupost</a>
                    <a href="<?= base_url() ?>projectes.html" class="btn btn-2 submit">projectes</a>
                </div>
            </li>
            
            <li data-transition="parallaxvertical">
                <img src="<?= base_url() ?>img/slider-img/2.jpg"  alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption sfb tp-resizeme carpenters-ul type2" 
                    data-x="left" data-hoffset="620" 
                    data-y="255" data-voffset="470" 
                    data-whitespace="nowrap"
                    data-start="1500">
                    <ul class="nav">
                        <li><a href="#">Disseny</a></li>
                        <li><a href="#">Comercial</a></li>
                        <li><a href="#">Industrial</a></li>
                    </ul>
                </div>
                <div class="tp-caption sfr tp-resizeme carpenters-h1 type2" 
                    data-x="left" data-hoffset="620" 
                    data-y="320" data-voffset="160" 
                    data-whitespace="nowrap"
                    data-start="900">
                    <span>Dissenyem</span> amb fusta<br><span>els</span> sonmis
                </div>
            </li>
        </ul>
    </div>
</section>