<?= $output ?>
<script>
$(document).ready(function() {
        $(document).on('change','#field-carreras_id, #field-sedes_id',function(e) {
            if($(this).val()!=''){
                    e.stopPropagation();
                    var selectedValue = $('#field-carreras_id').val();					
                    $.post('ajax_extension/programacion_carreras_id/carreras_id/'+encodeURI(selectedValue.replace(/\//g,'_agsl_')), {carreras_id:selectedValue,sedes_id:$("#field-sedes_id").val()}, function(data) {
                    var $el = $('#field-programacion_carreras_id');
                              var newOptions = data;
                              $el.empty(); // remove old options
                              $el.append($('<option></option>').attr('value', '').text(''));
                              $.each(newOptions, function(key, value) {
                                $el.append($('<option></option>')
                                   .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                                });
                              //$el.attr('selectedIndex', '-1');
                              $el.chosen().trigger('liszt:updated');

            },'json');
            $('#field-programacion_carreras_id').change();
        }
        });
        
        $(document).ready(function() {
                $(document).on('change refresh','#field-plan_estudio_id,#field-cursos_id',function(e) {
                    if($(this).val()!=''){
                            e.stopPropagation();
                            getMateriasPlan();
                    }
                });
        });
        
        if(typeof($('#field-plan_estudio_id').val())!=='undefined'){
            getMateriasPlan();
        }
});
function getMateriasPlan(){
    var selectedValue = $('#field-plan_estudio_id').val();
            var val = $("#field-materias_plan_id").val();
            $.post('ajax_extension/materias_plan_id/plan_estudio_id/'+encodeURI(selectedValue.replace(/\//g,'_agsl_')), {plan_estudio_id:selectedValue,cursos_id:$("#field-cursos_id").val()}, function(data) {					
            var $el = $('#field-materias_plan_id');
                      var newOptions = data;
                      $el.empty(); // remove old options
                      $el.append($('<option></option>').attr('value', '').text(''));
                      $.each(newOptions, function(key, value) {
                        var element = $('<option></option>').attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' '));
                        if(val===key){
                            element.attr('selected','selected');
                        }
                        $el.append(element);
                        });
                      //$el.attr('selectedIndex', '-1');
                      $el.chosen().trigger('liszt:updated');

    },'json');
    $('#field-materias_plan_id').change();
}
</script>