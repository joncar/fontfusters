<!--breadcrumb-->
<section class="row header-breadcrumb" style="background: url(<?= base_url() ?>img/about/blog.jpg)">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle"><?= $detail->titulo ?></h2>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="active"><?= $detail->titulo ?></li>
            </ol>
        </div>
    </div>
</section>


<!--blog-details-->
<section class="row blog_content">
    <div class="container">
        <div class="row sectpad">
            <div class="blog_section col-lg-8">
                <div class="row blog blog-details">
                    <div class="featured_img row m0">
                        <img src="<?= $detail->foto ?>" alt="" class="img-responsive">                    
                    </div>
                    <div class="post-contents row m0">
                        <a href="blog-details.html" class="post-date"><?= strftime("%d",strtotime($detail->fecha)); ?><span><?= strftime("%b",strtotime($detail->fecha)); ?></span></a>
                        <h4 class="post-title"><a href="blog-details.html"><?= $detail->titulo ?></a></h4>
                        <ul class="post-meta nav">
                            <li><i class="fa fa-user"></i>Por: <a href="#"><?= $detail->user ?></a></li>
                            <li><i class="fa fa-tag"></i><a href="#"><?= $detail->categoria->blog_categorias_nombre ?></a></li>
                        </ul>
                        <?= $detail->texto ?>
                    </div>
                </div> <!--Blog-->
                <div class="row m0 posts-social">
                    <div class="post-count">Compartir</div>
                    <ul class="nav social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    </ul>
                </div>
            </div>
            <?php $this->load->view('frontend/aside'); ?>
        </div>
    </div>
</section>