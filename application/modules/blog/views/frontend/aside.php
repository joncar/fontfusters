<div class="sidebar_section col-lg-4">
    <div class="sidebar row m0">
        <div class="row widget widget-search">
            <div class="row widget-inner">
                <form action="<?= base_url('blog') ?>" class="search-form" id="searchForm" method="get">
                    <div class="input-group">
                        <input type="search" class="form-control" name="direccion" placeholder="Buscar...">
                        <input type="hidden" id="page" name="page" value="<?= !empty($_GET['page'])?$_GET['page']:'1' ?>">
                        <input type="hidden" id="blog_categorias_id" name="blog_categorias_id" value="<?= !empty($_GET['blog_categorias_id'])?$_GET['blog_categorias_id']:'' ?>">
                        <span class="input-group-addon">
                            <button type="submit"><i class="icon icon-Search"></i></button>
                        </span>
                    </div>
                </form>
            </div>
        </div> <!--Search-->
        <div class="row widget widget-categories">
            <h4 class="widget-title">Categorias (<?= $categorias->num_rows() ?>)</h4>
            <div class="row widget-inner">
                <ul class="nav categories">
                    <?php if($categorias->num_rows()==0): ?>
                    <li>
                        <a href="#">Sin categorías</a>
                    </li>
                    <?php endif ?>
                    <?php foreach($categorias->result() as $c): ?>
                        <li><a href="javascript:changeCategoria(<?= $c->id ?>)"><i class="fa fa-angle-right"></i><?= $c->blog_categorias_nombre ?>(<?= $c->cantidad ?>)</a></li>
                    <?php endforeach ?>                    
                </ul>
            </div>
        </div> <!--Categories-->
        <?php if(!empty($populares)): ?>
            <div class="row widget widget-popular-posts">
                <h4 class="widget-title">Post més vistos</h4>
                <div class="row widget-inner">
                    <?php foreach($populares->result() as $d): ?>
                        <div class="media popular-post">
                            <div class="media-left">
                                <a href="<?= $d->link ?>"><img src="<?= $d->foto ?>" alt=""></a>
                            </div>
                            <div class="media-body">
                                <h5 class="post-title"><a href="<?= $d->link ?>"><?= $d->titulo ?></a></h5>
                                <h5 class="post-date"><a href="<?= $d->link ?>"><?= strftime("%d %M",strtotime($d->fecha)); ?></a></h5>                            
                            </div>
                        </div> <!--Popular Post-->
                    <?php endforeach ?>
                </div>
            </div> <!--Popular Posts-->
        <?php endif ?>
        <div class="row widget text-widget-post">
                       <!-- <h4 class="widget-title">Text Widget</h4>
                        <div class="text-widegt widget-inner">
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusant ium dolore que laudantium, totam rem aperiam, eaque ipsa quae ab illo inve ntore veritatis et quasi arc</p>
                        </div>-->
                    </div> <!--Tag Clouds-->
        <?php if(!empty($detail->tags)): ?>
            <div class="row widget widget-tag-clouds">
                <h4 class="widget-title">tags</h4>
                <div class="row widget-inner clouds">
                    <?php foreach(explode(',',$detail->tags) as $t): ?>
                        <a href="#" class="widget-tag"><?= $t ?></a>
                    <?php endforeach ?> 
                </div>
            </div> <!--Tag Clouds-->
         <?php endif ?>
    </div>
</div>
<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchForm").submit();
    }
    
    function changeCategoria(id){
        $("#blog_categorias_id").val(id);
        $("#searchForm").submit();
    }
</script>