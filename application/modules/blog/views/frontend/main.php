<!--breadcrumb-->
<section class="row header-breadcrumb" style="background: url(<?= base_url() ?>img/about/blog.jpg)">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">noticies</h2>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">inici</a></li>
                <li class="active">Noticies</li>
            </ol>
        </div>
    </div>
</section>


<!--blog-details-->
<section class="row blog_content">
    <div class="container">
        <div class="row sectpad">
            <div class="blog_section col-lg-8">
                <!--Blog-->

                <?php foreach ($detail->result() as $d): ?>
                    <div class="row blog">
                        <?php if(!empty($d->foto)): ?>
                                <div class="featured_img row m0">
                                   <a href="<?= $d->link ?>"><img src="<?= $d->foto ?>" alt="" class="img-responsive"></a>                    
                               </div>
                        <?php endif ?>
                        <div class="post-contents row m0">
                            <a href="<?= $d->link ?>" class="post-date"><?= strftime("%d",strtotime($d->fecha)); ?><span><?= strftime("%b",strtotime($d->fecha)); ?></span></a>
                            <h4 class="post-title"><a href="<?= $d->link ?>"><?= $d->titulo ?> </a></h4>
                            <ul class="post-meta nav">
                                <li><i class="fa fa-user"></i>Per: <a href="#"><?= $d->user ?></a></li>
                                <li><i class="fa fa-tag"></i><a href="#"><?= $d->categoria->blog_categorias_nombre ?></a></li>
                            </ul>
                            <p><?= substr(strip_tags($d->texto),0,80).'...' ?></p>
                            <a href="<?= $d->link ?>" class="read-more submit">lleguir Mes</a>
                        </div>
                    </div>
                <?php endforeach ?>       
                
                <ul class="pagination">
                    <?php for($i=1;$i<=$total_pages;$i++): ?>
                        <li><a href='javascript:changePage("<?= $i ?>")' class="<?= $i==$_GET['page']?'active':'' ?>"><?= $i ?></a></li>
                    <?php endfor ?>
                </ul>
            </div>
            <?php $this->load->view('frontend/aside'); ?>
        </div>
    </div>
</section>