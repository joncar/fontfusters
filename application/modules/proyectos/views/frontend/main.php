<!--breadcrumb-->
<section class="row header-breadcrumb" style="background: url(<?= base_url() ?>img/about/projectes.jpg)">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Projectes</h2>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">inici</a></li>
                <li class="active">Projectes</li>
            </ol>
        </div>
    </div>
</section>

<!--checkout-->
<section class="row check-project">
    <div class="container">
        <div class="row m0 section_header color">
            <h2>Mira els nostres projectes</h2>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dict eaque ipsa quae ab illo inventore veritatis et quasi architecto.</p>
        </div>
    </div>
</section>
<!--project-post-->
<section class="row latest_projects sectpad projects-1">
    <div class="container">
        <?php foreach($categorias as $c): ?>
            <div class="row projects2 popup-gallery">
                <div class="grid-sizer"></div>
                <?php foreach($c as $d): ?>
                    <div class="col-xs-6 project indoor wood_supply">
                        <div class="project-img">
                                <img src="<?= $d->foto_portada ?>" alt="">                        
                                <div class="project-text">
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="<?= base_url('projectes/'.toURL($d->id.'-'.$d->titulo)) ?>"><i class="icon icon-Linked"></i></a>
                                        </li>
                                        <li>
                                            <a href="<?= $d->foto_portada ?>" data-source="<?= base_url('projectes/'.toURL($d->id.'-'.$d->titulo)) ?>" title="<?= $d->titulo ?>" data-desc="<?= $d->categoria ?>" class="popup">
                                                <i class="icon icon-Search"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="row m0">
                                        <a href="<?= base_url('projectes/'.toURL($d->id.'-'.$d->titulo)) ?>"><h3><?= $d->titulo ?></h3></a>
                                        <p><?= $d->categoria ?></p>
                                    </div>
                               </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endforeach ?>
    </div>    
</section>