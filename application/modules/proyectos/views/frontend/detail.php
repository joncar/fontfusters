<!--breadcrumb-->
<section class="row header-breadcrumb" style="background: url(<?= base_url() ?>img/about/projectes.jpg)">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle"><?= $detail->titulo ?></h2>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li><a href="<?= site_url('projectes') ?>">Projectes</a></li>
                <li class="active"><?= $detail->titulo ?></li>
            </ol>
        </div>
    </div>
</section>

<!--projects-description-area-->

<section class="row projects-description-area">
    <div class="container">
        <div class="row main_photos">
            <div class="col-sm-7">                
                <img src="<?= $detail->foto_portada ?>" alt="" class="img-responsive">
            </div>
            <div class="col-sm-5">                
                <img src="<?= $detail->foto_portada2 ?>" alt="" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 projects-description">
                <div class="row m0 section_header project-top">
                    <h2>Descripció del projecte</h2>
                    <?= $detail->descripcion1 ?>
                </div>
            </div>

            <!--<div class="col-sm-5 projects-description-left">

                <div class="projects_hours">
                    <div class="row m0 section_header">
                        <h2>Detalls del projecte</h2>
                    </div>
                    <ul class="nav">
                        <li>Data d'inici  </li>
                        <li><?= $detail->fecha_inicio ?> </li>
                        <li>Data de finalització </li>
                        <li><?= $detail->fecha_fin ?> </li>
                    
                        <li>Client</li>
                        <li><?= $detail->cliente ?></li>
                        
                    </ul>
                </div>

            </div>-->
        </div>
        <div class="row project-gallery">
            <div class="gallery-area">
                <div class="gallery-sizer"></div>
                <div class="gallery-single col-xs-5">
                    <img src="<?= $detail->foto1 ?>" alt="">
                </div>
                <div class="gallery-single col-xs-7">
                    <img src="<?= $detail->foto3 ?>" alt="">
                </div>
                <div class="gallery-single col-xs-5">
                    <img src="<?= $detail->foto2 ?>" alt="">
                </div>
                <div class="gallery-single col-xs-12">
                    <img src="<?= $detail->foto4 ?>" alt="">
                </div>
            </div>
        </div>
    </div>
</section>