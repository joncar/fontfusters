<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function proyectos(){
            $crud = $this->crud_function('','');
            $crud->columns('titulo','categoria');
            $crud->set_field_upload('foto_portada','img/proyectos');
            $crud->set_field_upload('foto_portada2','img/proyectos');
            $crud->set_field_upload('foto1','img/proyectos');
            $crud->set_field_upload('foto2','img/proyectos');
            $crud->set_field_upload('foto3','img/proyectos');
            $crud->set_field_upload('foto4','img/proyectos');
            $crud->display_as('foto_portada','Foto de portada (670x350)')
                    ->display_as('foto_portada2','Foto de portada secundaria (470x350)')
                    ->display_as('foto1','Foto de portada (437x485)')
                    ->display_as('foto2','Foto de portada (437x485)')
                    ->display_as('foto3','Foto de portada (727x977)')
                    ->display_as('foto4','Foto de portada (1172x608)');
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
