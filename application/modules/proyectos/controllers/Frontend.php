<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        public function index(){            
            $output = array();
            $categorias = $this->db->get('proyectos');
            foreach($categorias->result() as $n=>$v){
                $categorias->row($n)->foto_portada = base_url('img/proyectos/'.$v->foto_portada); 
                $categorias->row($n)->foto_portada2 = base_url('img/proyectos/'.$v->foto_portada2); 
            }
            $output['view'] = 'frontend/main';
            $output['title'] = 'Projectes';
            
            
            //Sacar un array de 2 en 2
            $array = array();
            $x = 0;
            $i = 0;
            foreach ($categorias->result() as $c){
                $x++;
                $array[$i][] = $c;
                if($x==2){
                    $i++;
                }
            }
            $output['categorias'] = $array;
            $this->loadView($output);
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $proyectos = new Bdsource();
                $proyectos->where('id',$id);
                $proyectos->init('proyectos',TRUE);
                $this->proyectos->link = site_url('proyectos/'.toURL($this->proyectos->id.'-'.$this->proyectos->titulo));               
                $this->proyectos->foto_portada = base_url('img/proyectos/'.$this->proyectos->foto_portada);
                $this->proyectos->foto_portada2 = base_url('img/proyectos/'.$this->proyectos->foto_portada2);
                $this->proyectos->foto1 = base_url('img/proyectos/'.$this->proyectos->foto1);                
                $this->proyectos->foto2 = base_url('img/proyectos/'.$this->proyectos->foto2);                
                $this->proyectos->foto3 = base_url('img/proyectos/'.$this->proyectos->foto3);                
                $this->proyectos->foto4 = base_url('img/proyectos/'.$this->proyectos->foto4);                
                
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->proyectos,
                        'title'=>$this->proyectos->titulo                        
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
    }
?>
