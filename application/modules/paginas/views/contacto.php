<!--breadcrumb-->
<section class="row header-breadcrumb" style="background: url(<?= base_url() ?>img/about/contacto.jpg)">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Contacte</h2>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="active">contacte</li>
            </ol>
        </div>
    </div>
</section>

<section class="row touch">
    <div class="sectpad touch_bg">
        <div class="container">
            <div class="row m0 section_header color">
                <h2>Estem en contacte</h2>
                <p>Si vostè està interessat en saber més sobre com treballem o té alguna feina a fer, si us plau ompli el següent formulari o truqui'ns en qualsevol moment de 9h a 18h, de dilluns a divendres.</p>
            </div>

            <div class="row touch_middle">
                <div class="col-md-4 open_hours">
                    <div class="row m0 touch_top">
                        <ul class="nav">
                            <li class="item">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <i class="fa fa-map-marker"></i>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        Avinguda d'Europa, 6, Pol. Industrial Les Comes, 08700 Igualada, Barcelona
                                    </div>
                                </div>
                            </li>
                            <li class="item">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <i class="fa fa-envelope-o"></i>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        info@fontfusters.cat
                                        presupost@fontfusters.cat
                                    </div>
                                </div>
                            </li>
                            <li  class="item">
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#">
                                            <i class="fa fa-phone"></i>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        +34 93 803 51 31<br> +34 93 803 51 31
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 input_form">
                    <form action="<?= base_url('paginas/frontend/contacto') ?>" method="post" id="contactForm">
                        <input type="text" class="form-control" id="yourname" name="nombre" placeholder="Nom i Cognoms">
                        <input type="email" class="form-control" id="youremail" name="email" placeholder="Email">
                        <input type="text" class="form-control" id="subject" name="titulo" placeholder="Tema">
                        <textarea class="form-control" rows="6" id="message" name="comentario" placeholder="Missatge"></textarea>
                        <div class="row m0">
                            <button type="submit" class="btn btn-default submit">Enviar</button>
                        </div>
                    </form>
                    <?php
                        if(!empty($_SESSION['msj'])){
                            echo $_SESSION['msj'];
                            unset($_SESSION['msj']);
                        }
                    ?>
                    
                </div>
            </div>           
        </div>
    </div>
</section>
<!--MapBox-->
<section class="map">
    <div id="mapBox" class="row m0" data-lat="41.5923234" data-lon="1.6261614999999665" data-zoom="16" data-icon="<?= base_url() ?>img/marker.png"></div>
</section>