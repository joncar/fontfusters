<!--breadcrumb-->
<section class="row header-breadcrumb" style="background: url(<?= base_url() ?>img/about/pressupost.jpg)">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Pressupost</h2>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="active">Pressupost</li>
            </ol>
        </div>
    </div>
</section>



<section id="checkout-content" class="shop-page-content">
    <div class="container">
        <div class="row">
            <!--<div class="col-lg-12 return-customer">
                <p>Returning customer? <a href="#">Click here to login</a></p>
            </div>-->
        </div>

        <div class="row">
            [output]
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 col-xs-offset-0">
                <div class="row section_header">
                    <h2>Ens has demanat</h2>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-lg-12 order-box">
                        <ul>
                            <li id="nombreLabel">
                                El teu nom                         
                            </li>
                            <li id="apellidoLabel">
                                El teu cognom
                            </li>
                            <li id="emailLabel">
                                El teu E-mail
                            </li>
                            <li id="telefonoLabel">
                                La teu telèfon
                            </li>
                            <li id="empresaLabel">
                                
                            </li>
                            <li id="direccionLabel">
                                La teva direcció                                
                            </li>                            
                            <li id="direccion2Label"></li>
                            <li>Missatge <span>responen en 24h</span></li>
                            <li class="total bold" id="mensajeLabel" >                                
                            </li>
                            <!--<li><input type="radio"> Direct Bank Payment
                                <div class="note">
                                    <div class="i fa fa-caret-up"></div>
                                    Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.
                                </div>
                            </li>-->
                      <!--      <li><input type="radio"> Check Payment</li>
                            <li><input type="radio"> PayPal <img src="images/product-details/paypal.png" alt=""> <a href="#"><span>What is PayPal?</span></a></li>-->
                            <!--<li><a href="#" class="place-order">Enviar</a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
