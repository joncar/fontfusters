<?php $this->load->view('includes/template/banner'); ?>
<!--experiance-area-->
<section class="row experience-area">
   <div class="container">
       <div class="row">
           <div class="col-sm-5 worker-image">
               <img src="<?= base_url() ?>img/expreence/1.png" alt="">
           </div>
           <div class="col-sm-7 experience-info">
              <div class="content">
                  <h2>MÉS DE 150 ANYS TREBALLANT LA FUSTA</h2>
                  <p>Quatre generacions dedicades en exclusiva al món de la fusta. Més de 150 anys treballant la fusta, millorant-la, perfeccionant-la i innovant!</p>  
              </div>
              <ul class="content-list">
                  <li><a href="#">Fabricació de mobles d'interiors</a></li>
                  <li><a href="#">Rehabilitació de vivendes</a></li>
                  <li><a href="#">Treballs d'ebanisteria clàssica</a></li>
                  <li><a href="#">Dissenys d'interiors</a></li>
                  <li><a href="#">Productes d'alta qualitat</a></li>
                  
              </ul>
              <div class="content-image">
              <img src="<?= base_url() ?>img/expreence/2.jpg" alt=""> 
              </div>
               
           </div>
       </div>
   </div>
</section>


<!--we-do-->
<section class="row sectpad we-do-section">
    <div class="container">
        <div class="row m0 section_header color">
           <h2>què fem a font fusters?</h2>
        </div>
        <div class="we-do-slider">
            <div class="we-sliders">
                <div class="item">
                    <div class="post-image">
                        <img src="<?= base_url() ?>img/we-do/1.jpg"  alt="">
                    </div>
                    <a href="<?= base_url() ?>serveis/1-hosteleria.html"><h4>Tancaments</h4></a>
                    <p>Realitzem tot tipus de tancaments tan interior com exteriors</p>
                </div>
                <div class="item">
                    <div class="post-image">
                        <img src="<?= base_url() ?>img/we-do/2.jpg"  alt="">
                    </div>
                    <a href="<?= base_url() ?>serveis/2-particular-i-cuines.html"><h4>Mobiliari</h4></a>
                    <p>Treballem i fabriquem en fusta en tot tipus de mobiliari: armaris, taules, prestatges...</p>
                </div>
                <div class="item">
                    <div class="post-image">
                        <img src="<?= base_url() ?>img/we-do/3.jpg"  alt="">
                    </div>
                    <a href="<?= base_url() ?>serveis/3-restauraci--.html"><h4>Revestiments</h4></a>
                    <p>Fabriquem i instal·lem tot tipus de revestiments per terres, parets i sostres</p>
                </div>
                <div class="item">
                    <div class="post-image">
                        <img src="<?= base_url() ?>img/we-do/4.jpg"  alt="">
                    </div>
                    <a href="<?= base_url() ?>serveis/4-rehabilitaci--.html"><h4>Rehabilitació i restauració</h4></a>
                    <p>Tenim personal especialitzat en rehabilitació de mobiliari antic i deteriorat</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Projects -->
<section class="row latest_projects sectpad projects-1"  id='ultimsprojectes'>
    <div class="container">
        <div class="row m0 section_header">
            <h2>Els notres Últims projectes</h2>
        </div>
        <div class="row m0 filter_row">
            <ul class="nav project_filter" id="project_filter2">
                <li class="active" data-filter="*">Tots</li>
                <?php foreach($proyectos->result() as $p): ?>                    
                    <li data-filter=".a<?= $p->id ?>"><?= $p->titulo ?></li>
                <?php endforeach ?>
            </ul>
        </div>
        <div class="projects2 popup-gallery" id="projects">
            <div class="grid-sizer"></div>
            
            <?php foreach($proyectos->result() as $p): ?>
                    <div class="col-sm-6 project a<?= $p->id ?> wood_supply">
                        <div class="project-img">
                            <img src="<?= $p->foto_portada ?>" alt="">
                            <div class="project-text">
                                <ul class="list-unstyled">
                                    <li><a href="<?= $p->link ?>"><i class="icon icon-Linked"></i></a></li>
                                    <li><a href="<?= $p->foto_portada ?>" data-source="<?= $p->link ?>" title="<?= $p->titulo ?>" data-desc="<?= $p->categoria ?>" class="popup"><i class="icon icon-Search"></i></a></li>
                                </ul>
                                <div class="row m0">
                                    <a href="<?= $p->link ?>"><h3><?= $p->titulo ?></h3></a>
                                    <p><?= $p->categoria ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 project a<?= $p->id ?> wood_supply">
                        <div class="project-img">
                            <img src="<?= $p->foto_portada2 ?>" alt="">
                            <div class="project-text">
                                <ul class="list-unstyled">
                                    <li><a href="<?= $p->link ?>"><i class="icon icon-Linked"></i></a></li>
                                    <li><a href="<?= $p->foto_portada2 ?>" data-source="<?= $p->link ?>" title="<?= $p->titulo ?>" data-desc="<?= $p->categoria ?>" class="popup"><i class="icon icon-Search"></i></a></li>
                                </ul>
                                <div class="row m0">
                                    <a href="<?= $p->link ?>"><h3><?= $p->titulo ?></h3></a>
                                    <p><?= $p->categoria ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="col-sm-6 project a<?= $p->id ?> wood_supply">
                        <div class="project-img">
                            <img src="<?= $p->foto1?>" alt="">
                            <div class="project-text">
                                <ul class="list-unstyled">
                                    <li><a href="<?= $p->link ?>"><i class="icon icon-Linked"></i></a></li>
                                    <li><a href="<?= $p->foto1 ?>" data-source="<?= $p->link ?>" title="<?= $p->titulo ?>" data-desc="<?= $p->categoria ?>" class="popup"><i class="icon icon-Search"></i></a></li>
                                </ul>
                                <div class="row m0">
                                    <a href="<?= $p->link ?>"><h3><?= $p->titulo ?></h3></a>
                                    <p><?= $p->categoria ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="col-sm-6 project a<?= $p->id ?> wood_supply">
                        <div class="project-img">
                            <img src="<?= $p->foto2 ?>" alt="">
                            <div class="project-text">
                                <ul class="list-unstyled">
                                    <li><a href="<?= $p->link ?>"><i class="icon icon-Linked"></i></a></li>
                                    <li><a href="<?= $p->foto2 ?>" data-source="<?= $p->link ?>" title="<?= $p->titulo ?>" data-desc="<?= $p->categoria ?>" class="popup"><i class="icon icon-Search"></i></a></li>
                                </ul>
                                <div class="row m0">
                                    <a href="<?= $p->link ?>"><h3><?= $p->titulo ?></h3></a>
                                    <p><?= $p->categoria ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="col-sm-6 project a<?= $p->id ?> wood_supply">
                        <div class="project-img">
                            <img src="<?= $p->foto3 ?>" alt="">
                            <div class="project-text">
                                <ul class="list-unstyled">
                                    <li><a href="<?= $p->link ?>"><i class="icon icon-Linked"></i></a></li>
                                    <li><a href="<?= $p->foto3 ?>" data-source="<?= $p->link ?>" title="<?= $p->titulo ?>" data-desc="<?= $p->categoria ?>" class="popup"><i class="icon icon-Search"></i></a></li>
                                </ul>
                                <div class="row m0">
                                    <a href="<?= $p->link ?>"><h3><?= $p->titulo ?></h3></a>
                                    <p><?= $p->categoria ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <div class="col-sm-6 project a<?= $p->id ?> wood_supply">
                        <div class="project-img">
                            <img src="<?= $p->foto4 ?>" alt="">
                            <div class="project-text">
                                <ul class="list-unstyled">
                                    <li><a href="<?= $p->link ?>"><i class="icon icon-Linked"></i></a></li>
                                    <li><a href="<?= $p->foto4 ?>" data-source="<?= $p->link ?>" title="<?= $p->titulo ?>" data-desc="<?= $p->categoria ?>" class="popup"><i class="icon icon-Search"></i></a></li>
                                </ul>
                                <div class="row m0">
                                    <a href="<?= $p->link ?>"><h3><?= $p->titulo ?></h3></a>
                                    <p><?= $p->categoria ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php endforeach ?>
            
        </div>
    </div>
</section>

<!--work-shop-->
<section class="row fluid-work-area">
    <div class="work-image">
        <img src="<?= base_url() ?>img/workshop/work.jpg" alt="">
    </div>
    <div class="work-promo">
        <div class="promo-content">
            <h2>DISTRIBUÏDOR OFICIAL DE FICHET</h2>
            <p>Com a distrubuïdors oficials ens encarreguem que la teva porta mantingui uns nivells de seguretat i fiabilitat excepcionals.</p>
            <h3>Més de 30 anys d'experiència en seguretat</h3>
            <ul class="nav">
                <li>Portes blindades d'alta seguretat</li>
                <li>Panys Fortissime de seguretat</li>
                <li>Cilindro Fichet F3D de seguridad</li>
                <li>Fitxet confident. Caixes fortes</li>
            </ul>
        </div>
    </div>
</section>
<!--testimonial-->
<section class="row sectpad testimonial-area">
   <div class="container">
       <div class="row m0 section_header common">
           <h2>els nostres clients opinen</h2>
        </div>
        <div class="testimonial-sliders">
            <div class="item">
                <div class="media testimonial">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/testimonial/1.jpg"  alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <p>Ens vam comprar una casa i tenia molt clar el que volia. A Font Fuster van saber agafar la idea i han fet una feina molt professional. Estic encantat!</p>
                        <a href="#">-  Jordi Martí</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="media testimonial">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/testimonial/2.jpg"  alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <p>Vaig heredar un armari de la meva família però necessitava molt d'arreglo. A Font Fuster me l'han deixat com nou. Podrà durar dues generacions més segur!</p>
                        <a href="#">-  Joana Sala</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="media testimonial">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/testimonial/3.jpg"  alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href="#">-  John Michale</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="media testimonial">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/testimonial/4.jpg"  alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href="#">-  John Michale</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="media testimonial">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/testimonial/5.jpg"  alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href="#">-  Joana Vidal</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="media testimonial">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/testimonial/6.jpg"  alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipis cing elit, sed do eiusmod tempor incid idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
                        <a href="#">-  Miquel Sánchez</a>
                    </div>
                </div>
            </div>
        </div>
   </div>
</section>

<!-- latest-news-area -->
<section class="row sectpad latest-news-area">
    <div class="container">
        <div class="row m0 section_header">
           <h2>Últimes notícies</h2>
        </div>
        <div class="row latest-content">
            
            <?php foreach($blog->result() as $b): ?>
                <div class="col-sm-4 clo-xs-12 latest">
                    <div class="row m0 latest-image">
                        <a href="<?= $b->link ?>"><img src="<?= $b->foto ?>" alt=""></a>
                        <div class="latest-info-date">
                            <a href="<?= $b->link ?>">
                                <h3>
                                    <?= strftime("%d",strtotime($b->fecha)); ?> <small><?= strftime("%b",strtotime($b->fecha)); ?></small>
                                </h3>
                            </a>
                        </div>
                    </div>
                    <div class="latest-news-text">
                        <a href="<?= $b->link ?>">
                            <h4><?= $b->titulo ?></h4>
                        </a>
                        <p><?= substr(strip_tags($b->texto),0,80).'...' ?></p>
                        <div class="row m0 latest-meta">
                            <a href="<?= $b->link ?>">
                                <i class="fa fa-user"></i><?= $b->user ?>
                            </a> 
                            <a class="read_more" href="<?= $b->link ?>">
                                <i class="fa fa-comments"></i> Comments: <?= $b->comentarios ?>
                            </a>
                        </div>
                    </div>
                </div>    
            <?php endforeach ?>
        </div>
    </div>
</section>


<!-- clients -->
<!-- 
<section class="row clients">
    <div class="container">
        <div class="row m0 section_header">
            <h2>Els nostres clients</h2>
        </div>
        <div class="row clients-logos">
            <div class="col-md-2 col-sm-3 col-xs-6 client">
                <div class="row m0 inner-logo">
                   <a href="#"><img src="<?= base_url() ?>img/clients-logo/1.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6 client">
                <div class="row m0 inner-logo">
                   <a href="#"><img src="<?= base_url() ?>img/clients-logo/2.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6 client">
                <div class="row m0 inner-logo">
                   <a href="#"><img src="<?= base_url() ?>img/clients-logo/3.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6 client">
                <div class="row m0 inner-logo">
                   <a href="#"><img src="<?= base_url() ?>img/clients-logo/4.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6 client">
                <div class="row m0 inner-logo">
                   <a href="blog-details.html"><img src="<?= base_url() ?>img/clients-logo/5.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6 client">
                <div class="row m0 inner-logo">
                   <a href="blog-details.html"><img src="<?= base_url() ?>img/clients-logo/6.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section>
 -->
