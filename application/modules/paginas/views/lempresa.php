<!--breadcrumb-->
<section class="row header-breadcrumb" style="background: url(<?= base_url() ?>img/about/lempresa.jpg)">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">l'empresa</h2>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="active">l'empresa</li>
            </ol>
        </div>
    </div>
</section>

<!--who-are-->
<section class=" row who-area sectpad">
    <div class="container">
        <div class="row m0 section_header color">
            <h2>Qui som?</h2>
        </div>
        <div class="row">
            <div class="col-sm-4 col-lg-3 who-are">
                <div class="who-are-image row m0">
                    <img src="<?= base_url() ?>img/who-area/1.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-8 col-lg-9 who-are-texts">
                <div class="who-text">
                    <h3>Quatre generacions treballant la fusta</h3>
                    <p>Font Fusters, S.L és una empresa jove però de llarga tradició, sent la quarta generació de fusters.

<br>L’actual empresa, fundada l’any 1996, es dedica tant a la fusteria (interior i exterior) com a l’ebenisteria en general, de taller i d’obra.


L’ofici ens ve de lluny. <br>Actualment som la 4a generació de fusters. La fusteria va començar a Sant Martí de Tous, a Cal Fuster de la Plaça, una antiga casa situada al casc antic del milenari poble de Tous, que actualment és una casa rural (prem a Cal Fuster de la Plaça per anar al web de la casa rural)</p>
                    <!-- 
<div class="row m0">
                        <ul class="two-col-list nav">
                            <li>Natus erroroluptatem</li>
                            <li>Natus erroroluptatem</li>
                            <li>Accusantium doloremue</li>
                            <li>Accusantium doloremue</li>
                            <li>Laudantium unde</li>
                            <li>Laudantium unde</li> 
                        </ul>
                    </div>
 -->
                </div>
                <div class="who-text-box row m0 hidden-xs hidden-sm">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img src="<?= base_url() ?>img/who-area/box-image.png" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <p>Som un equip de més de 15 fusters en un taller de 200m2. Disposem de maquinària qualificatada per a treballar fustes d'alta qualitat i per a treballar petits detalls de retocs i reparació.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="who-text-box row m0 visible-xs visible-sm">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img src="<?= base_url() ?>img/who-area/box-image.png" alt="">
                    </a>
                </div>
                <div class="media-body">
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusant ium dolore mque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae voluptatem accus antium dolore mque laudantium, totam dict eaque ipsa.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--features-->
<section class="row features-section">
    <div class="features-image">
        <img src="<?= base_url() ?>img/features/1.jpg" alt="">
    </div>
    <div class="features-area">
        <div class="features">
            <div class="features-content">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/features/phone.png" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Horaris amplis i flexibles</h4>
                        <p>El nostre horari és de 9h del matí a 18h de la tarda. <br>Dissabtes tenim tancat.</p>
                    </div>
                </div>
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/features/icon.png" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Pressupostos sense compromís</h4>
                        <p>No dubtis a consultar-nos, et farem el pressupost adaptat a les teves necessitats i sense compromís.</p>
                    </div>
                </div>
            </div>
            <div class="features-content">
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/features/icon2.png" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Treballs certificats ISO 9010</h4>
                        <p>Tots els nostres treballs s'entreguen amb un certificat europeu de qualitat tècnica i material amb certificat ecològic.</p>
                    </div>
                </div>
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img src="<?= base_url() ?>img/features/hand.png" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">PREUS MOLT COMPETITIUS</h4>
                        <p>Portem molts anys dins l'ofici i els nostres proveedors són de confiança. Ens ajustem a cada client i les seves necessitats.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>

<!--team-->
<section class="row sectpad team-area">
    <div class="container">
        <div class="row m0 section_header color">
            <h2>El nostre equip</h2>
        </div>
        <div class="row our-team">
            <div class="col-sm-6 col-md-3 team">
                <div class="team-images row m0">
                    <img src="<?= base_url() ?>img/team/1.png" alt="">
                </div>
                <ul class="nav social-icons">
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                </ul>
                <div class="team-content">
                    <a href="#"><h4>Antoni Font</h4></a>
                    <p>Gerent</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 team">
                <div class="team-images row m0">
                    <img src="<?= base_url() ?>img/team/2.png" alt="">
                </div>
                <ul class="nav social-icons">
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                </ul>
                <div class="team-content">
                    <a href="#"><h4>Maria Luisa</h4></a>
                    <p>Administració</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 team">
                <div class="team-images row m0">
                    <img src="<?= base_url() ?>img/team/3.png" alt="">
                </div>
                <ul class="nav social-icons">
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                </ul>
                <div class="team-content">
                    <a href="#"><h4>Marc Esteve</h4></a>
                    <p>Comercial</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-3 team">
                <div class="team-images row m0">
                    <img src="<?= base_url() ?>img/team/4.png" alt="">
                </div>
                <ul class="nav social-icons">
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                    <li><a href="#"><i class="fa fa-skype"></i></a></li>
                </ul>
                <div class="team-content">
                    <a href="#"><h4>Joan Perez</h4></a>
                    <p>Cap de Fusters</p>
                </div>
            </div>
        </div>
    </div>
</section>