<form onsubmit="return save()" action="" method="post">
    <div id='contentHTML'>
            <?= $page ?>
    </div>
</form>
<script src="http://code.jquery.com/jquery-1.10.0.js"></script>		
<script src="<?= base_url() ?>/assets/grocery_crud/texteditor/tiny_mce/tinymce.min.js"></script>
<script src="<?= base_url() ?>/assets/grocery_crud/texteditor/tiny_mce/jquery.tinymce.js"></script>
<script>
    tinymce.init({
        selector: '#contentHTML',
        theme:'modern',
        image_advtab: true, 
        toolbar1: 'undo redo | formatselect fontselect fontsizeselect | bold italic underline forecolor backcolor | alignleft aligncenter alignright alignjustify |searchreplace spellchecker | table image filemanager | fullscreen',
        plugins: "submit code spellchecker searchreplace, table, image, imagetools, textcolor, filemanager, fullscreen",
        images_upload_base_path: '/img/uploads',
        valid_elements : "a[href|target=_blank],strong/b,div[align],br,form,span",
        menu:{
            file: {title: 'Archivo', items: 'submit'},
            format: {title: 'Formato', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
            view: {title: 'Ver', items: 'visualblocks'},
            table: {title: 'Tabla', items: 'inserttable tableprops deletetable | cell row column'},
            tools:{title:'Herramientas',items:'code spellchecker spellcheckerlanguage'}
        },
        allow_conditional_comments: true,
        allow_unsafe_link_target: true,
        relative_urls: true,
        convert_urls: false,
        cleanup_on_startup: false,
        trim_span_elements: false,
        verify_html: false,
        cleanup: false,
        inline: true,
        force_hex_style_colors:false,
        force_br_newlines:false,
        force_p_newlines:false,
        forced_root_block : ""
    });           
    function save(){        
        $("#contentHTML").find('a').removeAttr('data-mce-href');
        $("#contentHTML").find('img').removeAttr('data-mce-href');
        $("#contentHTML").find('img').removeAttr('data-mce-src');
        $("#contentHTML").find('br').removeAttr('data-mce-bogus');
        data = {data:$("#contentHTML").html()};
        $.post('<?= base_url('paginas/admin/paginas/edit/'.$name) ?>',data,function(data){
            alert('Guardado');
        });
    }
    
    function refresh(){
        document.location.reload();
    }        
</script>
    