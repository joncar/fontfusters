<!--breadcrumb-->
<section class="row header-breadcrumb" style="background: url(<?= base_url() ?>img/about/pressupost.jpg)">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Pressupost</h2>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">Inici</a></li>
                <li class="active">Pressupost</li>
            </ol>
        </div>
    </div>
</section>



<section id="checkout-content" class="shop-page-content">
    <div class="container">
        <div class="row">
            <!--<div class="col-lg-12 return-customer">
                <p>Returning customer? <a href="#">Click here to login</a></p>
            </div>-->
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 left-checkout">
                <div class="row m0 section_header">
                    <h2>Demana pressupost</h2>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label>Nom <span>*</span></label>
                        <input type="text" placeholder="">
                    </div>
                    <div class="col-lg-6">
                        <label>Cognoms <span>*</span></label>
                        <input type="text" placeholder="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label>Empresa</label>
                        <input type="text" placeholder="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label>Adreça <span>*</span></label>
                        <input type="text" placeholder="">
                        <input type="text" placeholder="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label>Localitat <span>*</span></label>
                        <input type="text" placeholder="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label>Província <span>*</span></label>
                        <div class="select-box">
                            <select class="select-menu" name="selectMenu">
                                <option value="default">Tria una opció</option>   
                                <option value="1">Barcelona</option> 
                                <option value="2">Tarragona</option> 
                                <option value="3">Lleida</option> 
                                <option value="4">Girona</option> 
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label>CP <span>*</span></label>
                        <input type="text" placeholder="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label>Email<span>*</span></label>
                        <input type="text" placeholder="">
                    </div>
                    <div class="col-lg-6">
                        <label>Telèfon <span>*</span></label>
                        <input type="text" placeholder="">
                    </div>
                </div>
               <!-- <div class="row">
                    <div class="col-lg-6">
                        <input type="checkbox"> <span>create an account</span>
                    </div>
                </div>-->
                <div class="row m0 section_header">
                    <h2>Explica'ns el teu projecte</h2>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <label>Missatge</label>
                        <textarea placeholder=""></textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 col-xs-offset-0">
                <div class="row section_header">
                    <h2>Ens has demanat</h2>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-lg-12 order-box">
                        <ul>
                            <li>Jordi Magaña<span></span></li>
                            <li>info@hipo.tv<span></span></li>
                            <li>President Lluis Companys, 29. 08700 Igualada <span class="bold"></span></li>
                            <li>Missatge <span>responen en 24h</span></li>
                            <li class="total"><span class="bold">Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunturNemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequunturfugit, sed quia consequuntur</span></li>
                            <!--<li><input type="radio"> Direct Bank Payment
                                <div class="note">
                                    <div class="i fa fa-caret-up"></div>
                                    Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.
                                </div>
                            </li>-->
                      <!--      <li><input type="radio"> Check Payment</li>
                            <li><input type="radio"> PayPal <img src="images/product-details/paypal.png" alt=""> <a href="#"><span>What is PayPal?</span></a></li>-->
                            <li><a href="#" class="place-order">Enviar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>