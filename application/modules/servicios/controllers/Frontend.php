<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        public function index(){
            $this->loadView(
                array(
                    'view'=>'frontend/detail',
                    'id'=>$this->db->get('servicios')->row()->id,
                    'title'=>'Serveis'
                ));
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $servicios = new Bdsource();
                $servicios->where('id',$id);
                $servicios->init('servicios',TRUE);
                $this->servicios->link = site_url('servicios/'.toURL($this->servicios->id.'-'.$this->servicios->titulo));
                $this->servicios->foto1 = base_url('img/servicios/'.$this->servicios->foto1);
                $this->servicios->foto2 = base_url('img/servicios/'.$this->servicios->foto2);
                $this->servicios->foto3 = base_url('img/servicios/'.$this->servicios->foto3);
                
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->servicios,
                        'title'=>$this->servicios->titulo,
                        'id'=>$id
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }

        public function soliciar_presupuesto($x = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('presupuestos')
                 ->set_theme('crud')
                 ->set_subject('Presupuesto')
                 ->unset_read()
                 ->unset_edit()
                 ->unset_export()
                 ->unset_print()
                 ->unset_delete()
                 ->unset_list()
                 ->set_field_upload('fichero','files')
                 ->required_fields_array();
            $crud->set_url('servicios/frontend/soliciar_presupuesto/');
            $crud->callback_after_insert(function($post){
                $this->enviarcorreo((object)$post,2,'font@fontfusters.cat');
            });
            $action = '';
            if(empty($x)){
                $action = 2;
            }
            $crud = $crud->render($action,'application/modules/servicios/views/');
            $crud->view = 'frontend/presupuesto';
            $this->loadView($crud);
        }
    }
?>
