<!--breadcrumb-->
<section class="row header-breadcrumb" style="background: url(<?= base_url() ?>img/about/servicios.jpg)">
    <div class="container">
            <div class="row m0 page-cover">
                <h2 class="page-cover-tittle">Serveis</h2>
            <ol class="breadcrumb">
                <li><a href="<?= site_url() ?>">inici</a></li>
                <li class="active">Serveis</li>
            </ol>
            </div>
        </div>
</section>

<!--services-2-->
<section class="row sectpad services">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 sidebar">
                <ul class="nav nav-tabs" role="tablist">
                    <?php foreach($this->db->get('servicios')->result() as $n=>$s): ?>                        
                        <li role="presentation" class="<?= $s->id==$id?'active':'' ?>">
                            <a href="#content-<?= $s->id ?>" aria-controls="content-<?= $s->id ?>" role="tab" data-toggle="tab">
                                <?= $s->titulo ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
                <!-- 
<div class="row m0 downloads">
                    <h4>Descarregar</h4>
                    <div class="dload">
                        <div class="dlbg">
                            <a href="#">Catàleg PDF</a>
                            <a href="#"><i class="fa fa-file-pdf-o"></i></a>
                        </div>
                    </div>
                    <div class="dload">
                        <div class="dlbg">
                            <a href="<?= site_url() ?>#ultimsprojectes" class="recent">Projectes Recents</a>
                            <a href="<?= site_url() ?>#ultimsprojectes"><i class="icon icon-Download"></i></a>
                        </div>
                    </div>
                </div>
 -->
            </div>
            <div class="col-sm-9 tab_pages">
                <div class="tab-content">
                    
                    <?php foreach($this->db->get('servicios')->result() as $n=>$s): ?>
                        <div role="tabpanel" class="tab-pane <?= $s->id==$id?'active':'' ?>" id="content-<?= $s->id ?>">
                                <div class="row m0 tab_inn_cont_1 ">
                                    <div class="tab_inn_cont_2 row">
                                        <div class="cont_left col-sm-8">
                                            <div class="row m0 section_header color">
                                                <h2><?= $s->titulo ?></h2> 
                                            </div>
                                            <?= $s->descripcion1 ?>
                                        </div>
                                        <div class="cont_right col-sm-4">
                                            <img src="<?= base_url('img/servicios/'.$s->foto1) ?>" alt="">                                
                                            <img src="<?= base_url('img/servicios/'.$s->foto2) ?>" alt="">
                                        </div>
                                    </div>
                                    <img src="<?= base_url('img/servicios/'.$s->foto3) ?>" alt="">                                
                                    <?= $s->descripcion2 ?>
                                </div>
                            </div>
                    <?php endforeach ?>
            </div>                     
        </div>
    </div>
</section>