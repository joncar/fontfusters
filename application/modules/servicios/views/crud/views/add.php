<?php
    $this->set_css('assets/grocery_crud/themes/bootstrap2/css/flexigrid.css');
	$this->set_js_lib('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
	$this->set_js_config('assets/grocery_crud/themes/bootstrap2/js/flexigrid-add.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 left-checkout flexigrid">
	<?php echo form_open( $insert_url, 'method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data"'); ?>
    <div class="row m0 section_header">
        <h2>Demana pressupost</h2>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <label>Nom <span>*</span></label>
            <input type="text" placeholder="" name="nombre" id="field-nombre">
        </div>
        <div class="col-lg-6">
            <label>Cognoms <span>*</span></label>
            <input type="text" placeholder="" name="apellido" id="field-apellido">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <label>Empresa</label>
            <input type="text" placeholder="" name="empresa" id="field-empresa">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <label>Adreça <span>*</span></label>
            <input type="text" placeholder="" name="direccion" id="field-direccion">
            <input type="text" placeholder="" name="direccion2" id="field-direccion2">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <label>Localitat <span>*</span></label>
            <input type="text" placeholder="" name="localidad" id="field-localidad">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <label>Província <span>*</span></label>
            <div class="select-box">
                <select class="select-menu" name="provincia" id="field-provincia">
                    <option value="default">Tria una opció</option>   
                    <option value="Barcelona">Barcelona</option> 
                    <option value="Tarragona">Tarragona</option> 
                    <option value="Lleida">Lleida</option> 
                    <option value="Girona">Girona</option> 
                </select>
            </div>
        </div>
        <div class="col-lg-6">
            <label>CP <span>*</span></label>
            <input type="text" placeholder="" name="cp" id="field-cp">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <label>Email<span>*</span></label>
            <input type="text" placeholder="" name="email" id="field-email">
        </div>
        <div class="col-lg-6">
            <label>Telèfon <span>*</span></label>
            <input type="text" placeholder="" name="telefono" id="field-telefono">
        </div>
    </div>
   <!-- <div class="row">
        <div class="col-lg-6">
            <input type="checkbox"> <span>create an account</span>
        </div>
    </div>-->
    <div class="row section_header" style="margin-top:50px;">
        <h2>Explica'ns el teu projecte</h2>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <label>Missatge</label>
            <textarea placeholder="" name="mensaje" id="field-mensaje"></textarea>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-12"><?php echo $input_fields['fichero']->input ?></div>
    </div>
	<div class="row">
		<div class="col-lg-12">
		    <div id='report-error' style="display:none" class='alert alert-danger'></div>
		    <div id='report-success' style="display:none" class='alert alert-success'></div>
		    <button id="form-button-save" type='submit' style="border:0px; background: #BC945E; text-transform: uppercase; display: inline-block; line-height: 36px; font-weight: 700; color: #fff; padding: 0 40px; margin-top: 30px;"><?php echo $this->l('form_save'); ?></button>
	    </div>
    </div>

    <?php
    foreach($hidden_fields as $hidden_field){
    	echo $hidden_field->input;
    }?>

    <?php echo form_close(); ?>
</div>

<script>

	var validation_url = '<?php echo $validation_url?>';

	var list_url = '<?php echo $list_url?>';



	var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";

	var message_insert_error = "<?php echo $this->l('insert_error')?>";

</script>