<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        public function index(){
            $this->loadView(
                array(
                    'view'=>'frontend/detail',
                    'customstyle'=>'style_fichets',
                    'id'=>$this->db->get('fichet')->row()->id,
                    'title'=>'Fichets'
                ));
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $fichet = new Bdsource();
                $fichet->where('id',$id);
                $fichet->init('fichet',TRUE);
                $this->fichet->link = site_url('fichet/'.toURL($this->fichet->id.'-'.$this->fichet->titulo));
                $this->fichet->foto1 = base_url('img/fichet/'.$this->fichet->foto1);
                $this->fichet->foto2 = base_url('img/fichet/'.$this->fichet->foto2);
                $this->fichet->foto3 = base_url('img/fichet/'.$this->fichet->foto3);
                
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'customstyle'=>'style_fichets',
                        'detail'=>$this->fichet,
                        'title'=>$this->fichet->titulo,
                        'id'=>$id
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
    }
?>
