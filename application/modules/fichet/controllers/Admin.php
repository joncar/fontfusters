<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        public function fichet(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto1','img/servicios');
            $crud->set_field_upload('foto2','img/servicios');
            $crud->set_field_upload('foto3','img/servicios');
            $crud->display_as('foto1','Foto (270x205)')
                     ->display_as('foto2','Foto (270x205)')
                     ->display_as('foto3','Foto (800x260)');
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
